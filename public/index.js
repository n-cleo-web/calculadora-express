const n1 = document.getElementById('numero1');
const n2 = document.getElementById('numero2');
const operador = document.getElementById('operador');
const calcular = document.getElementById('calcular');

function calc(e) {
    if (n1.value != '' && n2.value != '') {
        window.location.href = `http://localhost:3000/${operador.value}/${n1.value}/${n2.value}`
    } else {
        window.alert('Insira 2 números')
    }
}

calcular.addEventListener('click', calc);
calcular.addEventListener('touchstart', calc);