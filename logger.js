var moment = new Date();

const logger = function(req, res, next) {
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}: ${moment.toLocaleString('pt-BR')}`)
    next();
}

module.exports = logger;