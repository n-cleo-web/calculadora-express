const express = require("express");
const app = express();
const path = require ("path");
const logger = require('./logger.js');
const livereload = require("livereload");
const connectLivereload = require("connect-livereload");

const liveReloadServer = livereload.createServer();
liveReloadServer.watch(path.join(__dirname, 'public'));
liveReloadServer.server.once("connection", function() {
    setTimeout(function() {
        liveReloadServer.refresh("/");
    }, 100);
});

//app.use(logger);

app.use(connectLivereload());

app.get("/:operador/:numero1/:numero2", function(req, res) {
    var operador = req.params.operador;
    var n1 = parseFloat(req.params.numero1);
    var n2 = parseFloat(req.params.numero2);

    if (operador == 'adicionar') {
        var result = n1+n2;
    }

    if (operador == 'subtrair') {
        var result = n1-n2;
    }

    if (operador == 'multiplicar') {
        var result = n1*n2;
    }

    if (operador == 'dividir') {
        if (n2 != 0) {
            var result = n1/n2
        } else {
            result = 'Erro'
        }     
    }
    res.send(`
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Calculadora Express</title>
</head>
<body>
    <h1 id="resultado">Resultado: ${result}</h1>
    <button id="return">Retornar</button>
</body>
<script>
const retornar = document.getElementById('return');

function retorno(e) {
    window.location.href = 'http://localhost:3000/'
}

retornar.addEventListener('click', retorno);
retornar.addEventListener('touchstart', retorno);
</script>
</html>
    `);
 
})

app.use(express.static(path.join(__dirname, 'public')));

app.listen(3000, function() {
    console.log("Servidor rodando na porta 3000")
});